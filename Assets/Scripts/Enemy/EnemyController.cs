﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        [SerializeField] private float moveSpeed;
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

        private void Awake()
        {
            Debug.Assert(chasingThresholdDistance > 0,"chasingThreeshouldDistance has to be more than zero");
            Debug.Assert(moveSpeed > 0,"moveSpeed has to be more than zero");
        }

        private void MoveToPlayer()
        {
            
        }
    }    
}

