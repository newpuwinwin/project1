﻿ using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
 using Utillities;

 namespace Manager
 {
     public class GameManager : MonoSingleton<GameManager>
     {
         [SerializeField] private Button startButton;
         [SerializeField] private RectTransform dialog;
         [SerializeField] private PlayerSpaceship playerSpaceship;
         [SerializeField] private EnemySpaceship enemySpaceship;
         [SerializeField] private ScoreManager scoreManager;
         public event Action OnRestarted;
         [SerializeField] private int playerSpaceshipHp;
         [SerializeField] private int playerSpaceshipMoveSpeed;
         [SerializeField] private int enemySpaceshipHp;
         [SerializeField] private int enemySpaceshipMoveSpeed;
         [SerializeField] private PlayerSpaceship playerSpaceship2;
         
         public static GameManager Instance { get; private set; }
        
        
       
         private void Awake()
         {
             Debug.Assert(startButton != null, "startButton cannot be null");
             Debug.Assert(dialog != null, "dialog cannot be null");
             Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
             Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
             Debug.Assert(scoreManager != null, "scoreManager cannot be null");
             Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
             Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
             Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
             Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

             
             startButton.onClick.AddListener(OnStartButtonClicked);
         }

         private void OnStartButtonClicked()
         {
             dialog.gameObject.SetActive(false);
             StartGame();
         }

         private void StartGame()
         {
             scoreManager.Init(this);
             SpawnPlayerSpaceship();
             SpawnEnemySpaceship();
             SpawnPlayerSpaceship2();
         }

         private void SpawnPlayerSpaceship()
         {
             var spaceship = Instantiate(playerSpaceship);
             spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
             spaceship.OnExploded += OnPlayerSpaceshipExploded;
         }

         private void SpawnPlayerSpaceship2()
         {
             var spaceship = Instantiate(playerSpaceship2);
             spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
             spaceship.OnExploded += OnPlayerSpaceshipExploded;
         }
         private void OnPlayerSpaceshipExploded()
         {
             
             Restart();
         }

         private void SpawnEnemySpaceship()
         {
             var spaceship = Instantiate(enemySpaceship);
             spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
             spaceship.OnExploded += OnEnemySpaceshipExploded;
         }

         private void OnEnemySpaceshipExploded()
         {
             scoreManager.SetScore(1);
             Restart();
         }

         private void Restart()
         {
             DestroyRemainingShips();
             dialog.gameObject.SetActive(true);
             OnRestarted?.Invoke();
         }

         private void DestroyRemainingShips()
         {
             var remainingEnermies = GameObject.FindGameObjectsWithTag("Enermy");
             foreach (var enermy in remainingEnermies)
             {
                 Destroy(enermy);
             }


             {
                 var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
                 foreach (var player in remainingPlayers)
                 {
                     Destroy(player);
                 }
             }

         }
     }
 }