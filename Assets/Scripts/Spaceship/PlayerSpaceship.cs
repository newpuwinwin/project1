using System;
using UnityEngine;
using  UnityEngine.UI;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private float playerFireSoundVolume=0.3f;
        [SerializeField] private AudioClip playerFireSound;
        
        [SerializeField] private float playerDieSoundVolume=0.3f;
        [SerializeField] private AudioClip playerDieSound;
       
        [SerializeField] private int MaxHealth = 100;
        

        public Text HPText;

       

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
            HPText.text = $"Player HP:{Hp}";
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            HPText.text = $"{Hp}";
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerDieSound,Camera.main.transform.position,playerDieSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}